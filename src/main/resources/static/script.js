/*
function showInventory() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Stan Magazynu</h2>
        <button class="btn btn-primary mb-2" onclick="showAddInventoryForm()">Dodaj do Magazynu</button>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa Produktu</th>
                    <th>Ilość</th>
                    <th>Cena Kupna</th>
                    <th>Cena Sprzedaży</th>
                    <th>% Podatku</th>
                </tr>
            </thead>
            <tbody id="inventoryTableBody">
                <!-- Rows will be added dynamically -->
            </tbody>
        </table>
    `;
    fetchInventory();
}

function addInventory() {
    const productId = document.getElementById('productSelect').value;
    const quantity = document.getElementById('quantity').value;
    const purchasePrice = document.getElementById('purchasePrice').value;
    const salePrice = document.getElementById('salePrice').value;
    const taxPercentage = document.getElementById('taxPercentage').value;

    fetch('/api/inventory', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            product: { id: productId },
            quantity: quantity,
            purchasePrice: purchasePrice,
            salePrice: salePrice,
            taxPercentage: taxPercentage
        })
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }
            return response.json();
        })
        .then(data => {
            // Clear the error message
            document.getElementById('errorMessage').textContent = '';

            // Refresh the inventory list and dropdown
            fetchAvailableProductsForInventory();
            fetchInventory();  // Refresh the inventory display
        })
        .catch(error => {
            // Display the error message
            document.getElementById('errorMessage').textContent = error.message;
        });
}


function showAddInventoryForm() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Dodaj do Magazynu</h2>
        <form id="addInventoryForm">
            <div class="form-group">
                <label for="productSelect">Wybierz Produkt</label>
                <select class="form-control" id="productSelect"></select>
            </div>
            <div class="form-group">
                <label for="quantity">Ilość</label>
                <input type="number" class="form-control" id="quantity" required>
            </div>
            <div class="form-group">
                <label for="purchasePrice">Cena Kupna</label>
                <input type="number" class="form-control" id="purchasePrice" step="0.01" required>
            </div>
            <div class="form-group">
                <label for="salePrice">Cena Sprzedaży</label>
                <input type="number" class="form-control" id="salePrice" step="0.01" required>
            </div>
            <div class="form-group">
                <label for="taxPercentage">Podatek</label>
                <select class="form-control" id="taxPercentage">
                    <option value="23">23%</option>
                    <option value="8">8%</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Dodaj</button>
            <button type="button" class="btn btn-secondary" onclick="showInventory()">Wróć</button>
        </form>
        <div id="errorMessage" class="text-danger mt-2"></div>

        <h3>Aktualne Produkty w Magazynie</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa Produktu</th>
                    <th>Ilość</th>
                    <th>Cena Kupna</th>
                    <th>Cena Sprzedaży</th>
                    <th>% Podatku</th>
                </tr>
            </thead>
            <tbody id="inventoryTableBody">
                <!-- Rows will be added dynamically -->
            </tbody>
        </table>
    `;
    fetchAvailableProductsForInventory();
    fetchInventory();  // Fetch and display inventory
    document.getElementById('addInventoryForm').addEventListener('submit', function(event) {
        event.preventDefault();
        addInventory();
    });
}

function showProducts() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Wszystkie Produkty</h2>
        <button class="btn btn-primary mb-2" onclick="window.location.href='/add-product.html'">Dodaj Produkt</button>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa Produktu</th>
                    <th>Kategoria</th>
                </tr>
            </thead>
            <tbody>
                <!-- Rows will be added dynamically -->
            </tbody>
        </table>
    `;
    fetchProducts();
}

function showCategory(category) {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Kategoria: ${category}</h2>
        <button class="btn btn-primary mb-2" onclick="openAddProductForm('${category}')">Dodaj Produkt</button>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa Produktu</th>
                    <th>Kategoria</th>
                </tr>
            </thead>
            <tbody>
                <!-- Rows will be added dynamically -->
            </tbody>
        </table>
    `;
    fetchCategoryProducts(category);
}

function openAddProductForm(category) {
    window.location.href = `/add-product.html?category=${category}`;
}

function showSuppliers() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Dostawcy</h2>
        <button class="btn btn-primary mb-2" onclick="window.location.href='/add-supplier.html'">Dodaj Dostawcę</button>
        <table class="table">
            <thead>
                <tr>
                    <th>Nazwa Dostawcy</th>
                    <th>Email</th>
                    <th>Osoba Kontaktowa</th>
                    <th>Asortyment</th>
                </tr>
            </thead>
            <tbody id="suppliersTable">
                <!-- Rows will be added dynamically -->
            </tbody>
        </table>
    `;
    fetchSuppliers();
}

function fetchSuppliers() {
    fetch('/api/suppliers')
        .then(response => response.json())
        .then(data => {
            const tbody = document.getElementById('suppliersTable');
            tbody.innerHTML = ''; // Clear the table before adding new rows
            data.forEach(supplier => {
                const contactPerson = supplier.contactPerson ? supplier.contactPerson : ''; // Check if contactPerson is available
                const row = `<tr>
                    <td>${supplier.companyName}</td>
                    <td>${supplier.email}</td>
                    <td>${contactPerson}</td>
                    <td><button class="btn btn-primary" onclick="showSupplierProducts(${supplier.id}, '${supplier.companyName}')">Asortyment</button></td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        });
}

function showSupplierProducts(supplierId, companyName) {
    window.location.href = `/supplier-products.html?supplierId=${supplierId}&companyName=${companyName}`;
}

function fetchSupplierProducts() {
    const urlParams = new URLSearchParams(window.location.search);
    const supplierId = urlParams.get('supplierId');
    const companyName = urlParams.get('companyName');
    document.getElementById('supplierName').textContent = companyName;

    fetch(`/api/suppliers/${supplierId}/products`)
        .then(response => response.json())
        .then(data => {
            const tbody = document.getElementById('supplierProductsTable');
            tbody.innerHTML = ''; // Clear the table before adding new rows
            data.forEach(product => {
                const row = `<tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.category}</td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        });

    fetchProductsForDropdown();
}

function fetchAvailableProductsForSupplier(supplierId) {
    fetch(`/api/suppliers/${supplierId}/available-products`)
        .then(response => response.json())
        .then(data => {
            const select = document.getElementById('productSelect');
            select.innerHTML = ''; // Clear the dropdown before adding new options
            data.forEach(product => {
                const option = `<option value="${product.id}">${product.name}</option>`;
                select.insertAdjacentHTML('beforeend', option);
            });
        });
}


function fetchAvailableProductsForInventory() {
    fetch('/api/inventory/available-products')
        .then(response => response.json())
        .then(data => {
            const select = document.getElementById('productSelect');
            select.innerHTML = ''; // Clear the dropdown before adding new options
            data.forEach(product => {
                const option = `<option value="${product.id}">${product.name}</option>`;
                select.insertAdjacentHTML('beforeend', option);
            });
        });
}


function fetchProductsForDropdown() {
    fetch('/api/products')
        .then(response => response.json())
        .then(data => {
            const select = document.getElementById('productSelect');
            select.innerHTML = ''; // Clear the dropdown before adding new options
            data.forEach(product => {
                const option = `<option value="${product.id}">${product.name}</option>`;
                select.insertAdjacentHTML('beforeend', option);
            });
        });
}

function addProductToSupplier() {
    const urlParams = new URLSearchParams(window.location.search);
    const supplierId = urlParams.get('supplierId');
    const productId = document.getElementById('productSelect').value;

    fetch(`/api/suppliers/${supplierId}/products`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ id: productId })
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }
            return response.text();
        })
        .then(data => {
            // Clear the error message
            document.getElementById('errorMessage').textContent = '';

            // Reload the supplier products list and dropdown
            fetchSupplierProducts();
            fetchAvailableProductsForSupplier(supplierId);
        })
        .catch(error => {
            // Display the error message
            document.getElementById('errorMessage').textContent = error.message;
        });
}


function addSupplier() {
    const supplierName = document.getElementById('supplierName').value;
    const supplierEmail = document.getElementById('supplierEmail').value;
    const contactPerson = document.getElementById('contactPerson').value;
    const productSelect = document.getElementById('productSelect');
    const selectedProducts = Array.from(productSelect.selectedOptions).map(option => option.value);

    fetch('/api/suppliers', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            companyName: supplierName,
            email: supplierEmail,
            contactPerson: contactPerson,
            productList: selectedProducts.map(id => ({ id: id }))
        })
    })
        .then(response => response.json())
        .then(data => {
            window.location.href = '/index.html';
        });
}

function fetchInventory() {
    console.log('Fetching inventory');
    fetch('/api/inventory')
        .then(response => response.json())
        .then(data => {
            const tbody = document.getElementById('inventoryTableBody');
            tbody.innerHTML = ''; // Clear the table before adding new rows
            data.forEach(inventory => {
                const row = `<tr>
                    <td>${inventory.id}</td>
                    <td>${inventory.product.name}</td>
                    <td>${inventory.quantity}</td>
                    <td>${inventory.purchasePrice}</td>
                    <td>${inventory.salePrice}</td>
                    <td>${inventory.taxPercentage}</td>
                    <td>
                        <button class="btn btn-secondary" onclick="showEditInventoryForm(${inventory.id}, '${inventory.product.name}', ${inventory.quantity}, ${inventory.purchasePrice}, ${inventory.salePrice})">Edytuj</button>
                        <button class="btn btn-danger" onclick="deleteInventory(${inventory.id})">Usuń</button>
                    </td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        });
}
function showEditInventoryForm(id, productName, quantity, purchasePrice, salePrice) {
    document.getElementById('editInventoryId').value = id;
    document.getElementById('editProductName').value = productName;
    document.getElementById('editQuantity').value = quantity;
    document.getElementById('editPurchasePrice').value = purchasePrice;
    document.getElementById('editSalePrice').value = salePrice;
    document.getElementById('editInventoryModalLabel').textContent = `Edytuj Produkt: ${productName}`;
    $('#editInventoryModal').modal('show');
}


function updateInventory() {
    const id = document.getElementById('editInventoryId').value;
    const quantity = document.getElementById('editQuantity').value;
    const purchasePrice = document.getElementById('editPurchasePrice').value;
    const salePrice = document.getElementById('editSalePrice').value;

    fetch(`/api/inventory/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: id,
            quantity: quantity,
            purchasePrice: purchasePrice,
            salePrice: salePrice
        })
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }
            return response.json();
        })
        .then(data => {
            $('#editInventoryModal').modal('hide');
            fetchInventory();
        })
        .catch(error => {
            console.error('Error updating inventory:', error);
        });
}
function deleteInventory(id) {
    fetch(`/api/inventory/${id}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }
            fetchInventory();
        })
        .catch(error => {
            console.error('Error deleting inventory:', error);
        });
}

function fetchProducts() {
    fetch('/api/products')
        .then(response => response.json())
        .then(data => {
            const tbody = document.querySelector('table tbody');
            tbody.innerHTML = ''; // Clear the table before adding new rows
            data.forEach(product => {
                const row = `<tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.category}</td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        });
}

function fetchCategoryProducts(category) {
    fetch(`/api/products/category/${category}`)
        .then(response => response.json())
        .then(data => {
            const tbody = document.querySelector('table tbody');
            tbody.innerHTML = ''; // Clear the table before adding new rows
            data.forEach(product => {
                const row = `<tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.category}</td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        });
}

document.addEventListener('DOMContentLoaded', function() {
    const url = window.location.pathname;

    console.log('Page loaded:', url);

    if (url.includes('add-inventory.html')) {
        showAddInventoryForm();
    } else if (url.includes('index.html')) {
        fetchInventory();
    } else if (url.includes('supplier-products.html')) {
        const urlParams = new URLSearchParams(window.location.search);
        const supplierId = urlParams.get('supplierId');
        fetchSupplierProducts();
        fetchAvailableProductsForSupplier(supplierId);
        document.getElementById('addProductToSupplierForm').addEventListener('submit', function(event) {
            event.preventDefault();
            addProductToSupplier();
        });
    } else if (url.includes('add-supplier.html')) {
        fetchProductsForDropdown();
        document.getElementById('addSupplierForm').addEventListener('submit', function(event) {
            event.preventDefault();
            addSupplier();
        });
    }

    const urlParams = new URLSearchParams(window.location.search);
    const category = urlParams.get('category');
    if (category) {
        document.getElementById('productCategory').value = category;
    }

    const addProductForm = document.getElementById('addProductForm');
    if (addProductForm) {
        addProductForm.addEventListener('submit', function(event) {
            event.preventDefault();
            const productName = document.getElementById('productName').value;
            const productCategory = document.getElementById('productCategory').value;

            fetch('/api/products', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: productName,
                    category: productCategory
                })
            })
                .then(response => response.json())
                .then(data => {
                    window.location.href = '/index.html';
                });
        });
    }

    document.getElementById('editInventoryForm').addEventListener('submit', function(event) {
        event.preventDefault();
        updateInventory();
    });
});*/
