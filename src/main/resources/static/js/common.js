// common.js

function fetchAvailableProductsForInventory() {
    fetch('/api/inventory/available-products')
        .then(response => response.json())
        .then(data => {
            const select = document.getElementById('productSelect');
            select.innerHTML = ''; // Clear the dropdown before adding new options
            data.forEach(product => {
                const option = `<option value="${product.id}">${product.name}</option>`;
                select.insertAdjacentHTML('beforeend', option);
            });
        });
}

function fetchProductsForDropdown() {
    fetch('/api/products')
        .then(response => response.json())
        .then(data => {
            const select = document.getElementById('productSelect');
            select.innerHTML = ''; // Clear the dropdown before adding new options
            data.forEach(product => {
                const option = `<option value="${product.id}">${product.name}</option>`;
                select.insertAdjacentHTML('beforeend', option);
            });
        });
}
