let currentOrder = [];

document.addEventListener('DOMContentLoaded', function () {
    fetchSupplyOrders(); // Pobiera wszystkie zamówienia przy ładowaniu strony
});

function fetchSupplyOrders() {
    fetch('/api/supply-orders')
        .then(response => response.json())
        .then(data => {
            const tbody = document.getElementById('supplyOrdersTableBody');
            tbody.innerHTML = ''; // Wyczyść tabelę
            data.forEach(order => {
                const netValue = order.unitPrice * order.quantity;
                const vatValue = netValue * (order.vatRate / 100);
                const row = `<tr>
                    <td>${order.id}</td>
                    <td>${order.product.name}</td>
                    <td>${order.supplier.companyName}</td>
                    <td>${order.quantity}</td>
                    <td>${order.unitPrice}</td>
                    <td>${netValue.toFixed(2)}</td>
                    <td>${vatValue.toFixed(2)}</td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        });
}

function showAddSupplyOrderForm() {
    fetchProductsAndSuppliers();
    $('#addSupplyOrderModal').modal('show');
}

function fetchProductsAndSuppliers() {
    fetch('/api/products')
        .then(response => response.json())
        .then(products => {
            const productSelect = document.getElementById('productSelect');
            productSelect.innerHTML = ''; // Wyczyść poprzednie opcje
            products.forEach(product => {
                const option = document.createElement('option');
                option.value = product.id;
                option.textContent = product.name;
                productSelect.appendChild(option);
            });
            fetchSuppliersForProduct(products[0].id);
            fetchInventoryDetails(products[0].id);
        });

    document.getElementById('productSelect').addEventListener('change', function() {
        const productId = this.value;
        fetchSuppliersForProduct(productId);
        fetchInventoryDetails(productId);
    });
}

function fetchSuppliersForProduct(productId) {
    fetch(`/api/suppliers/product/${productId}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Błąd przy pobieraniu dostawców.');
            }
            return response.json();
        })
        .then(suppliers => {
            const supplierSelect = document.getElementById('supplierSelect');
            supplierSelect.innerHTML = ''; // Wyczyść poprzednie opcje
            suppliers.forEach(supplier => {
                const option = document.createElement('option');
                option.value = supplier.id;
                option.textContent = supplier.companyName;
                supplierSelect.appendChild(option);
            });
        })
        .catch(error => console.error('Błąd przy pobieraniu dostawców:', error));
}

function fetchInventoryDetails(productId) {
    fetch(`/api/inventory/product/${productId}`)
        .then(response => response.json())
        .then(inventory => {
            if (inventory) {
                document.getElementById('purchasePrice').value = inventory.purchasePrice; // Ustawienie ceny kupna
                document.getElementById('taxPercentage').value = inventory.taxPercentage; // Ustawienie podatku
                updateNetAndTax(); // Aktualizacja wartości netto i podatku
            } else {
                alert("Brak danych dla wybranego produktu w magazynie.");
            }
        })
        .catch(error => console.error('Błąd przy pobieraniu szczegółów magazynu:', error));
}

function updateNetAndTax() {
    const purchasePrice = parseFloat(document.getElementById('purchasePrice').value);
    const taxPercentage = parseFloat(document.getElementById('taxPercentage').value);
    const netValue = purchasePrice;
    const taxValue = purchasePrice * (taxPercentage / 100);

    document.getElementById('netValue').value = netValue.toFixed(2);
    document.getElementById('taxValue').value = taxValue.toFixed(2);
}

document.getElementById('addSupplyOrderForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const productId = document.getElementById('productSelect').value;
    const supplierId = document.getElementById('supplierSelect').value;
    const quantity = document.getElementById('quantity').value;
    const purchasePrice = document.getElementById('purchasePrice').value;
    const taxPercentage = document.getElementById('taxPercentage').value;

    const selectedProduct = document.getElementById('productSelect').selectedOptions[0].textContent;
    const selectedSupplier = document.getElementById('supplierSelect').selectedOptions[0].textContent;

    const supplyOrderItem = {
        product: { id: productId, name: selectedProduct },
        supplier: { id: supplierId, companyName: selectedSupplier },
        quantity: quantity,
        unitPrice: purchasePrice,
        vatRate: taxPercentage
    };

    currentOrder.push(supplyOrderItem);
    displayCurrentOrder();
    $('#addSupplyOrderModal').modal('hide');
});

function displayCurrentOrder() {
    const tbody = document.getElementById('addedProductsTableBody');
    tbody.innerHTML = ''; // Wyczyść tabelę
    let totalOrderValue = 0; // Zmienna do przechowywania całkowitej wartości zamówienia

    currentOrder.forEach((item, index) => {
        const netValue = item.unitPrice * item.quantity;
        const vatValue = netValue * (item.vatRate / 100);
        totalOrderValue += netValue + vatValue; // Dodawanie wartości netto i VAT do całkowitej sumy

        const row = `<tr>
            <td>${index + 1}</td>
            <td>${item.product.name}</td>
            <td>${item.supplier.companyName}</td>
            <td>${item.quantity}</td>
            <td>${item.unitPrice}</td>
            <td>${netValue.toFixed(2)}</td>
            <td>${vatValue.toFixed(2)}</td>
            <td><button class="btn btn-danger" onclick="removeItem(${index})">Usuń</button></td>
        </tr>`;
        tbody.insertAdjacentHTML('beforeend', row);
    });

    // Aktualizacja elementu wyświetlającego całkowitą wartość zamówienia
    document.getElementById('totalOrderValue').textContent = totalOrderValue.toFixed(2);
}

function removeItem(index) {
    currentOrder.splice(index, 1);
    displayCurrentOrder(); // Ponowne wyświetlenie zamówienia po usunięciu pozycji
}

function submitOrder() {
    const finalOrder = {
        batchDate: new Date().toISOString().split('T')[0], // Data zamówienia
        supplyOrders: currentOrder
    };

    fetch('/api/supply-order-batches', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(finalOrder)
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }
            return response.json();
        })
        .then(data => {
            currentOrder = []; // Resetuje bieżące zamówienie
            displayCurrentOrder(); // Opróżnia tabelę z zamówionymi produktami
            fetchSupplyOrders(); // Odświeża listę zamówień
            alert('Zamówienie zostało pomyślnie złożone!');
        })
        .catch(error => {
            console.error('Błąd przy składaniu zamówienia:', error);
            alert('Wystąpił błąd podczas składania zamówienia.');
        });
}
