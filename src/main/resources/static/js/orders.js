function fetchOrders() {
    fetch('/api/supply-order-batches')
        .then(response => response.json())
        .then(data => {
            const activeOrdersTableBody = document.getElementById('activeOrdersTableBody');
            const archivedOrdersTableBody = document.getElementById('archivedOrdersTableBody');
            activeOrdersTableBody.innerHTML = '';
            archivedOrdersTableBody.innerHTML = '';

            data.forEach(orderBatch => {
                const row = `<tr>
                    <td>${orderBatch.id}</td>
                    <td>${orderBatch.batchDate}</td>
                    <td>${orderBatch.orderType}</td>
                    <td>${orderBatch.status}</td>
                    <td><button class="btn btn-primary" onclick="viewOrderDetails(${orderBatch.id})">Szczegóły</button>
                     ${orderBatch.status === "ACTIVE" && orderBatch.orderType === "KUPNO"
                    ? `<button class="btn btn-success" onclick="confirmSettlement(${orderBatch.id})">Rozlicz Zamówienie</button>`
                    : ''
                        }
                     </td>
                </tr>`;

                if (orderBatch.status === "ACTIVE") {
                    activeOrdersTableBody.insertAdjacentHTML('beforeend', row);
                } else {
                    archivedOrdersTableBody.insertAdjacentHTML('beforeend', row);
                }
            });
        })
        .catch(error => console.error('Błąd przy pobieraniu zamówień:', error));
}

function viewOrderDetails(orderBatchId) {
    fetch(`/api/supply-order-batches/${orderBatchId}`)
        .then(response => response.json())
        .then(orderBatch => {
            const orderDetailsTableBody = document.getElementById('orderDetailsTableBody');
            const orderTotalAmountElement = document.getElementById('orderTotalAmount');
            orderDetailsTableBody.innerHTML = '';

            let totalOrderValue = 0;

            orderBatch.supplyOrders.forEach(order => {
                const netValue = order.unitPrice * order.quantity;
                const vatValue = netValue * (order.vatRate / 100);
                const totalValue = netValue + vatValue;

                totalOrderValue += totalValue;

                const row = `<tr>
                    <td>${order.product.name}</td>
                    <td>${order.supplier.companyName}</td>
                    <td>${order.quantity}</td>
                    <td>${order.unitPrice.toFixed(2)}</td>
                    <td>${netValue.toFixed(2)}</td>
                    <td>${vatValue.toFixed(2)}</td>
                </tr>`;
                orderDetailsTableBody.insertAdjacentHTML('beforeend', row);
            });

            // Ustawienie sumy zamówienia w elemencie HTML
            orderTotalAmountElement.textContent = totalOrderValue.toFixed(2);

            $('#orderDetailsModal').modal('show'); // Pokaż modal z szczegółami zamówienia
        })
        .catch(error => console.error('Błąd przy pobieraniu szczegółów zamówienia:', error));

}
function confirmSettlement(orderId) {
    if (confirm("Czy na pewno chcesz rozliczyć to zamówienie?")) {
        settleOrder(orderId);
    }
}

function settleOrder(orderId) {
    fetch(`/api/supply-order-batches/${orderId}/settle`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text); });
            }
            // Sprawdzenie, czy odpowiedź nie jest pusta przed próbą parsowania JSON-a
            return response.text().then(text => {
                if (text) {
                    return JSON.parse(text);
                }
                return {}; // Zwróć pusty obiekt, jeśli odpowiedź jest pusta
            });
        })
        .then(data => {
            alert('Zamówienie zostało pomyślnie rozliczone!');
            location.reload();  // Wymusza odświeżenie strony po rozliczeniu zamówienia
        })
        .catch(error => {
            console.error('Błąd:', error);
            alert('Wystąpił błąd podczas rozliczania zamówienia: ' + error.message);
        });
}

