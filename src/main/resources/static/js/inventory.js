// Funkcja do pobierania i wyświetlania wszystkich produktów w magazynie
function fetchInventory() {
    fetch('/api/inventory')
        .then(response => response.json())
        .then(data => {
            const tbody = document.getElementById('inventoryTableBody');
            tbody.innerHTML = '';  // Wyczyść tabelę przed dodaniem nowych wierszy
            data.forEach(item => {
                const row = `
                    <tr>
                        <td>${item.id}</td>
                        <td>${item.product.name}</td>
                        <td>${item.quantity}</td>
                        <td>${item.purchasePrice}</td>
                        <td>${item.salePrice}</td>
                        <td>${item.taxPercentage}</td>
                        <td>
                            <button class="btn btn-warning btn-sm" onclick="showEditInventoryForm(${item.id}, '${item.product.name}', ${item.quantity}, ${item.purchasePrice}, ${item.salePrice}, ${item.taxPercentage})">Modyfikuj</button>
                            <button class="btn btn-danger btn-sm" onclick="confirmDeleteInventory(${item.id}, '${item.product.name}')">Usuń</button>
                        </td>
                    </tr>
                `;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        })
        .catch(error => console.error('Error fetching inventory:', error));
}

// Funkcja do wyświetlania formularza do dodawania nowego produktu do magazynu
function showAddInventoryForm() {
    fetchAvailableProductsForDropdown(); // Wczytaj dostępne produkty do dropdowna
    const addModal = new bootstrap.Modal(document.getElementById('addInventoryModal'));
    addModal.show();
}

// Funkcja do pobierania dostępnych produktów do dropdowna
function fetchAvailableProductsForDropdown() {
    fetch('/api/inventory/available-products')
        .then(response => response.json())
        .then(data => {
            const select = document.getElementById('productSelect');
            select.innerHTML = ''; // Wyczyść dropdown przed dodaniem opcji
            data.forEach(product => {
                const option = `<option value="${product.id}">${product.name}</option>`;
                select.insertAdjacentHTML('beforeend', option);
            });
        })
        .catch(error => console.error('Error fetching available products:', error));
}

// Funkcja do dodawania nowego produktu do magazynu
document.getElementById('addInventoryForm').addEventListener('submit', function(event) {
    event.preventDefault();
    const productId = document.getElementById('productSelect').value;
    const quantity = document.getElementById('quantity').value;
    const purchasePrice = document.getElementById('purchasePrice').value;
    const salePrice = document.getElementById('salePrice').value;
    const taxPercentage = document.getElementById('taxPercentage').value;

    fetch('/api/inventory', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            product: { id: productId },
            quantity: quantity,
            purchasePrice: purchasePrice,
            salePrice: salePrice,
            taxPercentage: taxPercentage
        })
    })
        .then(response => response.json())
        .then(data => {
            // Zamknij modal po dodaniu produktu
            const addModal = bootstrap.Modal.getInstance(document.getElementById('addInventoryModal'));
            addModal.hide();

            // Odśwież listę magazynową po dodaniu nowego produktu
            fetchInventory();
        })
        .catch(error => {
            console.error('Error adding inventory:', error);
            alert('Wystąpił błąd podczas dodawania produktu do magazynu. Spróbuj ponownie.');
        });
});

// Funkcja do wyświetlania formularza do modyfikacji istniejącego produktu w magazynie
function showEditInventoryForm(id, productName, quantity, purchasePrice, salePrice, taxPercentage) {
    document.getElementById('editInventoryId').value = id;
    document.getElementById('editProductName').value = productName;
    document.getElementById('editQuantity').value = quantity;
    document.getElementById('editPurchasePrice').value = purchasePrice;
    document.getElementById('editSalePrice').value = salePrice;
    document.getElementById('editTaxPercentage').value = taxPercentage;
    const editModal = new bootstrap.Modal(document.getElementById('editInventoryModal'));
    editModal.show();
}

// Funkcja do zapisywania zmodyfikowanego produktu
document.getElementById('editInventoryForm').addEventListener('submit', function(event) {
    event.preventDefault();
    const id = document.getElementById('editInventoryId').value;
    const quantity = document.getElementById('editQuantity').value;
    const purchasePrice = document.getElementById('editPurchasePrice').value;
    const salePrice = document.getElementById('editSalePrice').value;
    const taxPercentage = document.getElementById('editTaxPercentage').value;

    fetch(`/api/inventory/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            quantity: quantity,
            purchasePrice: purchasePrice,
            salePrice: salePrice,
            taxPercentage: taxPercentage
        })
    })
        .then(response => response.json())
        .then(data => {
            const editModal = bootstrap.Modal.getInstance(document.getElementById('editInventoryModal'));
            editModal.hide();
            fetchInventory();
        })
        .catch(error => {
            console.error('Error updating inventory:', error);
            alert('Wystąpił błąd podczas aktualizacji produktu w magazynie. Spróbuj ponownie.');
        });
});

// Funkcja do potwierdzania usunięcia produktu z magazynu
function confirmDeleteInventory(id, productName) {
    if (confirm(`Czy na pewno chcesz usunąć produkt: ${productName}?`)) {
        deleteInventory(id);
    }
}

// Funkcja do usuwania produktu z magazynu
function deleteInventory(id) {
    fetch(`/api/inventory/${id}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }
            fetchInventory();  // Odśwież listę magazynową po usunięciu produktu
        })
        .catch(error => {
            console.error('Error deleting inventory:', error);
            alert('Wystąpił błąd podczas usuwania produktu z magazynu. Spróbuj ponownie.');
        });
}

// Automatycznie wywołaj pobranie listy produktów w magazynie po załadowaniu strony
document.addEventListener('DOMContentLoaded', fetchInventory);


