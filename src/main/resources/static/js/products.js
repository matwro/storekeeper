// Pokazywanie modala do dodawania nowego produktu
function showAddProductModal() {
    const addProductModal = new bootstrap.Modal(document.getElementById('addProductModal'));
    addProductModal.show();
}

// Dodawanie nowego produktu
function addNewProduct() {
    const name = document.getElementById('newProductName').value;
    const category = document.getElementById('newProductCategory').value;

    fetch('/api/products', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name: name, category: category })
    })
        .then(response => {
            if (response.ok) {
                fetchProducts(); // Odśwież listę produktów po dodaniu nowego
                const addProductModal = bootstrap.Modal.getInstance(document.getElementById('addProductModal'));
                addProductModal.hide();
            } else {
                alert('Wystąpił problem podczas dodawania produktu.');
            }
        })
        .catch(error => console.error('Error:', error));
}

// Potwierdzenie usunięcia produktu
function confirmDelete(productId, productName) {
    document.getElementById('deleteProductName').textContent = productName;

    const confirmDeleteBtn = document.getElementById('confirmDeleteBtn');
    confirmDeleteBtn.onclick = function () {
        deleteProduct(productId);
    };

    const deleteModal = new bootstrap.Modal(document.getElementById('deleteModal'));
    deleteModal.show();
}

// Usuwanie produktu
function deleteProduct(productId) {
    fetch(`/api/products/${productId}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (response.ok) {
                fetchProducts(); // Odśwież listę produktów po usunięciu
                const deleteModal = bootstrap.Modal.getInstance(document.getElementById('deleteModal'));
                deleteModal.hide();
            } else {
                alert('Wystąpił problem podczas usuwania produktu.');
            }
        })
        .catch(error => console.error('Error:', error));
}

// Edycja produktu
function editProduct(productId, productName) {
    document.getElementById('editProductName').value = productName;

    const saveEditBtn = document.getElementById('saveEditBtn');
    saveEditBtn.onclick = function () {
        saveProductEdits(productId);
    };

    const editModal = new bootstrap.Modal(document.getElementById('editModal'));
    editModal.show();
}

// Zapisanie zmian w produkcie
function saveProductEdits(productId) {
    const newName = document.getElementById('editProductName').value;

    fetch(`/api/products/${productId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name: newName })
    })
        .then(response => {
            if (response.ok) {
                fetchProducts(); // Odśwież listę produktów po edycji
                const editModal = bootstrap.Modal.getInstance(document.getElementById('editModal'));
                editModal.hide();
            } else {
                alert('Wystąpił problem podczas edytowania produktu.');
            }
        })
        .catch(error => console.error('Error:', error));
}

// Pobieranie produktów z backendu i filtrowanie ich według kategorii
function fetchProducts(category = 'all') {
    const url = category === 'all' ? '/api/products' : `/api/products/category/${category}`;

    fetch(url)
        .then(response => response.json())
        .then(data => displayProducts(data))
        .catch(error => console.error('Error:', error));
}

// Wyświetlanie produktów w tabeli
function displayProducts(products) {
    const productTableBody = document.getElementById('productTableBody');
    productTableBody.innerHTML = '';

    if (products.length === 0) {
        productTableBody.innerHTML = '<tr><td colspan="4">Brak produktów do wyświetlenia</td></tr>';
    } else {
        products.forEach(product => {
            const row = `
                <tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.category}</td>
                    <td>
                        <button class="btn btn-warning btn-sm" onclick="editProduct(${product.id}, '${product.name}')">Modyfikuj</button>
                        <button class="btn btn-danger btn-sm" onclick="confirmDelete(${product.id}, '${product.name}')">Usuń</button>
                    </td>
                </tr>
            `;
            productTableBody.insertAdjacentHTML('beforeend', row);
        });
    }
}

// Filtrowanie produktów przy zmianie kategorii
function filterProducts() {
    const selectedCategory = document.getElementById('categorySelect').value;
    fetchProducts(selectedCategory);
}

// Załadowanie wszystkich produktów przy starcie strony
document.addEventListener('DOMContentLoaded', function() {
    fetchProducts(); // Domyślnie ładowane są wszystkie produkty
});
