fetch('/api/accounts')
    .then(response => response.json())
    .then(accounts => {
        console.log(accounts);
        if (accounts.length > 0 && accounts[0].balance !== undefined) {
            document.getElementById('accountBalance').textContent = accounts[0].balance.toFixed(2);
        } else {
            document.getElementById('accountBalance').textContent = 'Brak danych';
        }
    })
    .catch(error => {
        console.error('Błąd przy pobieraniu stanu konta:', error);
        document.getElementById('accountBalance').textContent = 'Błąd';
    });
console.log("Account JS Loaded");

// Wywołanie funkcji po załadowaniu strony
document.addEventListener('DOMContentLoaded', function() {
    fetchAccountBalance();
});
