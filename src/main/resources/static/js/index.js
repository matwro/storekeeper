function loadPage(page) {
    $.ajax({
        url: page,
        type: 'GET',
        success: function(data) {
            $('#content').html(data);
            // Inicjalizacja skryptów po załadowaniu zawartości
            if (page.includes('inventory')) {
                fetchInventory();  // Inicjalizacja magazynu
            } else if (page.includes('products')) {
                fetchProducts();  // Inicjalizacja produktów
            } else if (page.includes('suppliers')) {
                fetchSuppliers();  // Inicjalizacja dostawców
            } else if (page.includes('supplyOrder')) {
                fetchSupplyOrders();
            } else if (page.includes('orders')) {
                fetchOrders();  // Inicjalizacja zamówień
            }

        },
        error: function() {
            $('#content').html('<p>Przepraszamy, nie można załadować strony.</p>');
        }
    });
}