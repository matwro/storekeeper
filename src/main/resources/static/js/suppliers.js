// Funkcja do pobierania i wyświetlania wszystkich dostawców
function fetchSuppliers() {
    fetch('/api/suppliers')
        .then(response => response.json())
        .then(data => {
            const tbody = document.getElementById('suppliersTableBody');
            tbody.innerHTML = '';
            data.forEach(supplier => {
                const row = `<tr>
                    <td>${supplier.id}</td> 
                    <td>${supplier.companyName}</td>
                    <td>${supplier.email}</td>
                    <td>${supplier.contactPerson}</td>
                    <td>
                        <button class="btn btn-primary" onclick="showEditSupplierForm(${supplier.id}, '${supplier.companyName}', '${supplier.email}', '${supplier.contactPerson}')">Edytuj</button>
                        <button class="btn btn-danger" onclick="showDeleteSupplierModal(${supplier.id}, '${supplier.companyName}')">Usuń</button>
                        <button class="btn btn-info supplier-products-btn" data-id="${supplier.id}" data-name="${supplier.companyName}">Asortyment</button>
                    </td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
            attachEvents(); // Przypisanie zdarzeń do przycisków
        });
}

// Funkcja do wyświetlania formularza do dodawania nowego dostawcy
function showAddSupplierForm() {
    const addModal = new bootstrap.Modal(document.getElementById('addSupplierModal'));
    addModal.show();
}

// Funkcja do dodawania nowego dostawcy
document.getElementById('addSupplierForm').addEventListener('submit', function(event) {
    event.preventDefault();
    const supplierName = document.getElementById('supplierName').value;
    const supplierEmail = document.getElementById('supplierEmail').value;
    const contactPerson = document.getElementById('contactPerson').value;

    fetch('/api/suppliers', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            companyName: supplierName,
            email: supplierEmail,
            contactPerson: contactPerson
        })
    })
        .then(response => response.json())
        .then(data => {
            const addModal = bootstrap.Modal.getInstance(document.getElementById('addSupplierModal'));
            addModal.hide();
            fetchSuppliers();
        })
        .catch(error => {
            console.error('Error adding supplier:', error);
            alert('Wystąpił błąd podczas dodawania dostawcy. Spróbuj ponownie.');
        });
});

// Funkcja do wyświetlania formularza do edycji dostawcy
function showEditSupplierForm(id, companyName, email, contactPerson) {
    document.getElementById('editSupplierId').value = id;
    document.getElementById('editSupplierName').value = companyName;
    document.getElementById('editSupplierEmail').value = email;
    document.getElementById('editContactPerson').value = contactPerson;
    const editModal = new bootstrap.Modal(document.getElementById('editSupplierModal'));
    editModal.show();
}

// Funkcja do zapisywania zmienionych danych dostawcy
function editSupplier() {
    const id = document.getElementById('editSupplierId').value;
    const supplierName = document.getElementById('editSupplierName').value;
    const supplierEmail = document.getElementById('editSupplierEmail').value;
    const contactPerson = document.getElementById('editContactPerson').value;

    fetch(`/api/suppliers/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            companyName: supplierName,
            email: supplierEmail,
            contactPerson: contactPerson
        })
    })
        .then(response => response.json())
        .then(data => {
            const editModal = bootstrap.Modal.getInstance(document.getElementById('editSupplierModal'));
            editModal.hide();
            fetchSuppliers();  // Odśwież listę dostawców po edycji
        })
        .catch(error => {
            console.error('Error updating supplier:', error);
            alert('Wystąpił błąd podczas aktualizacji dostawcy. Spróbuj ponownie.');
        });
}
// Przypisanie funkcji `editSupplier` do formularza edycji
document.getElementById('editSupplierForm').addEventListener('submit', function(event) {
    event.preventDefault();
    editSupplier();
});

// Funkcja do wyświetlania modal z potwierdzeniem usunięcia dostawcy
function showDeleteSupplierModal(id, companyName) {
    document.getElementById('deleteSupplierName').textContent = companyName;
    const confirmButton = document.getElementById('confirmDeleteSupplier');
    confirmButton.setAttribute('data-id', id);

    // Wyświetl modal
    const deleteModal = new bootstrap.Modal(document.getElementById('deleteSupplierModal'));
    deleteModal.show();
}

// Funkcja do usuwania dostawcy
function deleteSupplier() {
    const id = document.getElementById('confirmDeleteSupplier').getAttribute('data-id');

    fetch(`/api/suppliers/${id}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text); });
            }
            // Zamknij modal po udanym usunięciu
            const deleteModal = bootstrap.Modal.getInstance(document.getElementById('deleteSupplierModal'));
            deleteModal.hide();
            fetchSuppliers();  // Odśwież listę dostawców po usunięciu
        })
        .catch(error => {
            console.error('Error deleting supplier:', error);
            alert('Wystąpił błąd podczas usuwania dostawcy. Spróbuj ponownie.');
        });
}

// Przypisanie funkcji usunięcia do przycisku potwierdzającego usunięcie
document.getElementById('confirmDeleteSupplier').addEventListener('click', deleteSupplier)

// Funkcja do wyświetlania modal z asortymentem dostawcy
function showSupplierProductsModal(supplierId, companyName) {
    document.getElementById('supplierProductsModalLabel').textContent = `Asortyment Dostawcy: ${companyName}`;
    const supplierProductsModal = new bootstrap.Modal(document.getElementById('supplierProductsModal'));

    fetchAvailableProductsForSupplier(supplierId);
    fetchSupplierProducts(supplierId);

    document.getElementById('addProductToSupplierForm').addEventListener('submit', function(event) {
        event.preventDefault();
        addProductToSupplier(supplierId);
    });

    supplierProductsModal.show();
}
// Funkcja do pobierania i wyświetlania dostępnych produktów do dodania
function fetchAvailableProductsForSupplier(supplierId) {
    fetch(`/api/suppliers/${supplierId}/available-products`)
        .then(response => response.json())
        .then(data => {
            const select = document.getElementById('productSelect');
            select.innerHTML = ''; // Wyczyszczenie listy przed dodaniem nowych opcji
            data.forEach(product => {
                const option = `<option value="${product.id}">${product.name}</option>`;
                select.insertAdjacentHTML('beforeend', option);
            });
        });
}
// Funkcja do wyświetlania produktów przypisanych do dostawcy
function fetchSupplierProducts(supplierId) {
    fetch(`/api/suppliers/${supplierId}/products`)
        .then(response => response.json())
        .then(data => {
            const tbody = document.getElementById('supplierProductsTable');
            tbody.innerHTML = ''; // Wyczyszczenie tabeli przed dodaniem nowych danych
            data.forEach(product => {
                const row = `<tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.category}</td>
                    <td><button class="btn btn-danger" onclick="removeProductFromSupplier(${supplierId}, ${product.id})">Usuń</button></td>
                </tr>`;
                tbody.insertAdjacentHTML('beforeend', row);
            });
        });
}
// Funkcja do dodawania produktu do dostawcy
function addProductToSupplier(supplierId) {
    const productId = document.getElementById('productSelect').value;

    fetch(`/api/suppliers/${supplierId}/products`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ id: productId })
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text); });
            }
            fetchSupplierProducts(supplierId); // Odśwież listę produktów
            fetchAvailableProductsForSupplier(supplierId); // Odśwież listę dostępnych produktów
        })
        .catch(error => {
            document.getElementById('errorMessage').textContent = error.message;
        });
}

// Funkcja do usuwania produktu od dostawcy
function removeProductFromSupplier(supplierId, productId) {
    fetch(`/api/suppliers/${supplierId}/products/${productId}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text); });
            }
            fetchSupplierProducts(supplierId); // Odśwież listę produktów
            fetchAvailableProductsForSupplier(supplierId); // Odśwież listę dostępnych produktów
        })
        .catch(error => {
            document.getElementById('errorMessage').textContent = error.message;
        });
}

// Przykład wywołania modalu z asortymentem po kliknięciu przycisku
function attachEvents() {
    document.querySelectorAll('.supplier-products-btn').forEach(btn => {
        btn.addEventListener('click', function () {
            const supplierId = this.getAttribute('data-id');
            const companyName = this.getAttribute('data-name');
            showSupplierProductsModal(supplierId, companyName);
        });
    });
}

// Inicjalizacja eventów po załadowaniu strony
document.addEventListener('DOMContentLoaded', function() {
    attachEvents();
});

// Automatyczne wywołanie pobrania listy dostawców po załadowaniu strony
document.addEventListener('DOMContentLoaded', fetchSuppliers);
