package com.example.inventory_management.service;

import com.example.inventory_management.model.SupplyOrderBatch;
import com.example.inventory_management.repository.SupplyOrderBatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupplyOrderBatchService {

    @Autowired
    private SupplyOrderBatchRepository supplyOrderBatchRepository;

    public List<SupplyOrderBatch> getAllSupplyOrderBatches() {
        return supplyOrderBatchRepository.findAll();
    }

    public SupplyOrderBatch createSupplyOrderBatch(SupplyOrderBatch supplyOrderBatch) {
        supplyOrderBatch.getSupplyOrders().forEach(order -> order.setSupplyOrderBatch(supplyOrderBatch));
        return supplyOrderBatchRepository.save(supplyOrderBatch);
    }

    public SupplyOrderBatch getSupplyOrderBatchById(Long id) {
        Optional<SupplyOrderBatch> supplyOrderBatch = supplyOrderBatchRepository.findById(id);
        if (supplyOrderBatch.isPresent()) {
            return supplyOrderBatch.get();
        } else {
            throw new RuntimeException("SupplyOrderBatch not found");
        }
    }

    public void deleteSupplyOrderBatch(Long id) {
        supplyOrderBatchRepository.deleteById(id);
    }
    public SupplyOrderBatch updateSupplyOrderBatch(SupplyOrderBatch supplyOrderBatch) {
        return supplyOrderBatchRepository.save(supplyOrderBatch);
    }
}
