package com.example.inventory_management.service;

import com.example.inventory_management.model.Account;
import com.example.inventory_management.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Account createAccount(String name, BigDecimal initialBalance) {
        Account account = new Account(name, initialBalance);
        return accountRepository.save(account);
    }

    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    public Account getAccount(Long id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Account not found"));
    }

    public Account updateAccount(Long id, BigDecimal newBalance) {
        Account account = getAccount(id);
        account.setBalance(newBalance);
        return accountRepository.save(account);
    }

    public void deleteAccount(Long id) {
        accountRepository.deleteById(id);
    }

    public void increaseBalance(Long id, BigDecimal amount) {
        Account account = getAccount(id);
        account.increaseBalance(amount);
        accountRepository.save(account);
    }

    public void decreaseBalance(Long id, BigDecimal amount) {
        Account account = getAccount(id);
        account.decreaseBalance(amount);
        accountRepository.save(account);
    }
}
