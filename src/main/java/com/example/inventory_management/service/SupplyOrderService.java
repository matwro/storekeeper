package com.example.inventory_management.service;

import com.example.inventory_management.model.SupplyOrder;
import com.example.inventory_management.repository.SupplyOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplyOrderService {

    @Autowired
    private SupplyOrderRepository supplyOrderRepository;

    public SupplyOrder createSupplyOrder(SupplyOrder supplyOrder) {
        return supplyOrderRepository.save(supplyOrder);
    }

    public List<SupplyOrder> getAllSupplyOrders() {
        return supplyOrderRepository.findAll();
    }

    public SupplyOrder updateSupplyOrder(Long id, SupplyOrder updatedSupplyOrder) {
        SupplyOrder supplyOrder = supplyOrderRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Supply Order not found"));
        supplyOrder.setQuantity(updatedSupplyOrder.getQuantity());
        supplyOrder.setUnitPrice(updatedSupplyOrder.getUnitPrice());
        supplyOrder.setVatRate(updatedSupplyOrder.getVatRate());
        return supplyOrderRepository.save(supplyOrder);
    }

    public void deleteSupplyOrder(Long id) {
        supplyOrderRepository.deleteById(id);
    }
}