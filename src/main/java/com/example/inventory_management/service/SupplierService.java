package com.example.inventory_management.service;

import com.example.inventory_management.model.Product;
import com.example.inventory_management.model.Supplier;
import com.example.inventory_management.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupplierService {
    @Autowired
    private SupplierRepository supplierRepository;


    public Supplier getSupplierById(Long id) {
        return supplierRepository.findById(id).orElseThrow(() -> new RuntimeException("Supplier not found"));
    }

    public Supplier updateSupplier(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    public List<Supplier> getSuppliersByProduct(Product product) {
        return supplierRepository.findAll().stream()
                .filter(supplier -> supplier.getProductList().contains(product))
                .collect(Collectors.toList());
    }
}