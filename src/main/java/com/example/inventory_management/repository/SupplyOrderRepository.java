package com.example.inventory_management.repository;

import com.example.inventory_management.model.SupplyOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplyOrderRepository extends JpaRepository<SupplyOrder, Long> {
    // Możesz dodać dodatkowe metody zapytań w razie potrzeby
}