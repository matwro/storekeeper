package com.example.inventory_management.repository;

import com.example.inventory_management.model.SupplyOrderBatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplyOrderBatchRepository extends JpaRepository<SupplyOrderBatch, Long> {
}