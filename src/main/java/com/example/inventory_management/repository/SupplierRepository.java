package com.example.inventory_management.repository;

import com.example.inventory_management.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
    @Query("SELECT s FROM Supplier s JOIN s.productList p WHERE p.id = :productId")
    List<Supplier> findSuppliersByProductId(@Param("productId") Long productId);
}
