package com.example.inventory_management.controller;


import com.example.inventory_management.model.Product;
import com.example.inventory_management.model.Supplier;
import com.example.inventory_management.repository.ProductRepository;
import com.example.inventory_management.repository.SupplierRepository;
import com.example.inventory_management.service.ProductService;
import com.example.inventory_management.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/suppliers")
public class SupplierController {
    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Supplier> getAllSuppliers() {
        return supplierRepository.findAll();
    }

    @GetMapping("/{supplierId}/products")
    public ResponseEntity<List<Product>> getSupplierProducts(@PathVariable Long supplierId) {
        Optional<Supplier> supplier = supplierRepository.findById(supplierId);
        if (supplier.isPresent()) {
            return ResponseEntity.ok(supplier.get().getProductList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/{supplierId}/available-products")
    public List<Product> getAvailableProductsForSupplier(@PathVariable Long supplierId) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(supplierId);

        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            List<Product> supplierProducts = supplier.getProductList();
            List<Product> allProducts = productRepository.findAll();
            return allProducts.stream()
                    .filter(product -> !supplierProducts.contains(product))
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>(); // Return an empty list if supplier is not found
        }
    }

    @PostMapping
    public Supplier createSupplier(@RequestBody Supplier supplier) {
        return supplierRepository.save(supplier);
    }
    // Dodaj metodę do usuwania produktu od dostawcy
    @DeleteMapping("/{supplierId}/products/{productId}")
    public Supplier removeProductFromSupplier(@PathVariable Long supplierId, @PathVariable Long productId) {
        Supplier supplier = supplierService.getSupplierById(supplierId);
        Product product = productService.getProductById(productId);
        supplier.getProductList().remove(product);
        return supplierService.updateSupplier(supplier);
    }
    // Dodaj metodę do aktualizacji produktu u dostawcy
    @PutMapping("/{supplierId}/products/{productId}")
    public Supplier updateProductInSupplier(@PathVariable Long supplierId, @PathVariable Long productId, @RequestBody Product productDetails) {
        Supplier supplier = supplierService.getSupplierById(supplierId);
        supplier.getProductList().stream()
                .filter(product -> product.getId().equals(productId))
                .forEach(product -> {
                    product.setName(productDetails.getName());
                    product.setCategory(productDetails.getCategory());
                });
        return supplierService.updateSupplier(supplier);
    }
    // Metoda pomocnicza do pobrania dostawcy po ID
    @GetMapping("/{supplierId}")
    public Supplier getSupplierById(@PathVariable Long supplierId) {
        return supplierService.getSupplierById(supplierId);
    }

    @PostMapping("/{supplierId}/products")
    public ResponseEntity<String> addProductToSupplier(@PathVariable Long supplierId, @RequestBody Product product) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(supplierId);
        Optional<Product> productOptional = productRepository.findById(product.getId());

        if (supplierOptional.isPresent() && productOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            Product productEntity = productOptional.get();

            // Sprawdzenie, czy produkt już istnieje na liście
            if (supplier.getProductList().contains(productEntity)) {
                return ResponseEntity.badRequest().body("Product already exists in supplier's product list");
            }

            supplier.getProductList().add(productEntity);
            supplierRepository.save(supplier);
            return ResponseEntity.ok("Product added to supplier's product list");
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/{supplierId}")
    public ResponseEntity<Void> deleteSupplier(@PathVariable Long supplierId) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(supplierId);
        if (supplierOptional.isPresent()) {
            supplierRepository.delete(supplierOptional.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PutMapping("/{supplierId}")
    public ResponseEntity<Supplier> updateSupplier(@PathVariable Long supplierId, @RequestBody Supplier supplierDetails) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(supplierId);
        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            supplier.setCompanyName(supplierDetails.getCompanyName());
            supplier.setEmail(supplierDetails.getEmail());
            supplier.setContactPerson(supplierDetails.getContactPerson());
            // Zaktualizuj inne pola, jeśli istnieją
            Supplier updatedSupplier = supplierRepository.save(supplier);
            return ResponseEntity.ok(updatedSupplier);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/product/{productId}")
    public List<Supplier> getSuppliersForProduct(@PathVariable Long productId) {
        return supplierRepository.findSuppliersByProductId(productId);
    }
}

