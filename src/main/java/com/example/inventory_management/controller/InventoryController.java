package com.example.inventory_management.controller;

import com.example.inventory_management.model.Account;
import com.example.inventory_management.model.Inventory;
import com.example.inventory_management.model.Product;
import com.example.inventory_management.repository.AccountRepository;
import com.example.inventory_management.repository.InventoryRepository;
import com.example.inventory_management.repository.ProductRepository;
import com.example.inventory_management.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/inventory")
public class InventoryController {

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping
    public List<Inventory> getAllInventory() {
        return inventoryRepository.findAll();
    }


    @GetMapping("/available-products")
    public List<Product> getAvailableProductsForInventory() {
        List<Product> allProducts = productRepository.findAll();
        List<Product> inventoryProducts = inventoryRepository.findAll().stream()
                .map(Inventory::getProduct)
                .collect(Collectors.toList());
        return allProducts.stream()
                .filter(product -> !inventoryProducts.contains(product))
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<Inventory> addInventory(@RequestBody Inventory inventory) {
        Optional<Product> productOptional = productRepository.findById(inventory.getProduct().getId());

        if (productOptional.isPresent()) {
            Inventory savedInventory = inventoryRepository.save(inventory);
            return ResponseEntity.ok(savedInventory);
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PutMapping("/{id}")
    public Inventory updateInventory(@PathVariable Long id, @RequestBody Inventory updatedInventory) {
        Optional<Inventory> optionalInventory = inventoryRepository.findById(id);
        if (optionalInventory.isPresent()) {
            Inventory inventory = optionalInventory.get();
            inventory.setQuantity(updatedInventory.getQuantity());
            inventory.setPurchasePrice(updatedInventory.getPurchasePrice());
            inventory.setSalePrice(updatedInventory.getSalePrice());
            inventory.setTaxPercentage(updatedInventory.getTaxPercentage());
            return inventoryRepository.save(inventory);
        } else {
            throw new RuntimeException("Inventory not found");
        }
    }

    @DeleteMapping("/{id}")
    public void deleteInventory(@PathVariable Long id) {
        inventoryRepository.deleteById(id);
    }

    @GetMapping("/product/{productId}")
    public ResponseEntity<Inventory> getInventoryByProductId(@PathVariable Long productId) {
        Optional<Inventory> inventory = inventoryRepository.findByProductId(productId);
        if (inventory.isPresent()) {
            return ResponseEntity.ok(inventory.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/account-balance")
    public ResponseEntity<Double> getAccountBalance() {
        Optional<Account> account = accountRepository.findById(1L);  // Zakładamy, że mamy jedno konto z ID 1
        if (account.isPresent()) {
            return ResponseEntity.ok(account.get().getBalance().doubleValue());  // Konwersja BigDecimal na Double
        } else {
            return ResponseEntity.notFound().build();
        }

    }
}