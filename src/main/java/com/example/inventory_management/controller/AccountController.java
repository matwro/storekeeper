package com.example.inventory_management.controller;

import com.example.inventory_management.model.Account;
import com.example.inventory_management.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping
    public ResponseEntity<Account> createAccount(@RequestBody Map<String, Object> request) {
        String name = (String) request.get("name");
        BigDecimal initialBalance = new BigDecimal(request.get("balance").toString());
        Account account = accountService.createAccount(name, initialBalance);
        return ResponseEntity.ok(account);
    }

    @GetMapping
    public ResponseEntity<List<Account>> getAllAccounts() {
        List<Account> accounts = accountService.getAllAccounts();
        return ResponseEntity.ok(accounts);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> getAccountById(@PathVariable Long id) {
        Account account = accountService.getAccount(id);
        return ResponseEntity.ok(account);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Account> updateAccount(@PathVariable Long id, @RequestBody BigDecimal newBalance) {
        Account updatedAccount = accountService.updateAccount(id, newBalance);
        return ResponseEntity.ok(updatedAccount);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAccount(@PathVariable Long id) {
        accountService.deleteAccount(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/increase")
    public ResponseEntity<Account> increaseBalance(@PathVariable Long id, @RequestBody Map<String, BigDecimal> request) {
        BigDecimal amount = request.get("amount");
        accountService.increaseBalance(id, amount);
        return ResponseEntity.ok(accountService.getAccount(id));
    }

    @PutMapping("/{id}/decrease")
    public ResponseEntity<Account> decreaseBalance(@PathVariable Long id, @RequestBody Map<String, BigDecimal> request) {
        BigDecimal amount = request.get("amount");
        accountService.decreaseBalance(id, amount);
        return ResponseEntity.ok(accountService.getAccount(id));
    }
}
