package com.example.inventory_management.controller;

import com.example.inventory_management.model.*;
import com.example.inventory_management.repository.InventoryRepository;
import com.example.inventory_management.service.AccountService;
import com.example.inventory_management.service.SupplyOrderBatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/api/supply-order-batches")
public class SupplyOrderBatchController {

    @Autowired
    private SupplyOrderBatchService supplyOrderBatchService;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private AccountService accountService;

    @GetMapping
    public List<SupplyOrderBatch> getAllSupplyOrderBatches() {
        return supplyOrderBatchService.getAllSupplyOrderBatches();
    }

    @PostMapping
    public ResponseEntity<SupplyOrderBatch> createSupplyOrderBatch(@RequestBody SupplyOrderBatch supplyOrderBatch) {
        supplyOrderBatch.setOrderType(OrderType.KUPNO);
        supplyOrderBatch.setStatus(OrderStatus.ACTIVE);

        // Przypisanie batcha do każdego zamówienia
        for (SupplyOrder order : supplyOrderBatch.getSupplyOrders()) {
            order.setSupplyOrderBatch(supplyOrderBatch);
        }

        SupplyOrderBatch savedBatch = supplyOrderBatchService.createSupplyOrderBatch(supplyOrderBatch);
        return ResponseEntity.ok(savedBatch);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SupplyOrderBatch> getSupplyOrderBatchById(@PathVariable Long id) {
        SupplyOrderBatch batch = supplyOrderBatchService.getSupplyOrderBatchById(id);
        return ResponseEntity.ok(batch);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSupplyOrderBatch(@PathVariable Long id) {
        supplyOrderBatchService.deleteSupplyOrderBatch(id);
        return ResponseEntity.noContent().build();
    }
    @PostMapping("/{id}/settle")
    public ResponseEntity<Void> settleOrder(@PathVariable Long id) {
        SupplyOrderBatch batch = supplyOrderBatchService.getSupplyOrderBatchById(id);
        if (batch.getStatus() == OrderStatus.COMPLETED) {
            return ResponseEntity.badRequest().build(); // Zamówienie już rozliczone
        }

        batch.getSupplyOrders().forEach(order -> {
            // Znajdź odpowiedni wpis w magazynie
            Inventory inventory = inventoryRepository.findByProductId(order.getProduct().getId())
                    .orElseThrow(() -> new RuntimeException("Product not found in inventory"));

            // Aktualizuj ilość w magazynie
            inventory.setQuantity(inventory.getQuantity() + order.getQuantity());
            inventoryRepository.save(inventory);

            // Zaktualizuj saldo konta
            BigDecimal orderTotal = order.getUnitPrice().multiply(BigDecimal.valueOf(order.getQuantity()));
            accountService.decreaseBalance(1L, orderTotal); // Zakładamy, że mamy jedno konto o ID 1

        });

        // Zmień status zamówienia na COMPLETED
        batch.setStatus(OrderStatus.COMPLETED);
        supplyOrderBatchService.updateSupplyOrderBatch(batch);

        return ResponseEntity.ok().build();
    }

}
