package com.example.inventory_management.controller;

import com.example.inventory_management.model.SupplyOrder;
import com.example.inventory_management.repository.SupplyOrderRepository;
import com.example.inventory_management.service.SupplyOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/supply-orders")
public class SupplyOrderController {

    @Autowired
    private SupplyOrderRepository supplyOrderRepository;

    @Autowired
    private SupplyOrderService supplyOrderService;


    @GetMapping
    public List<SupplyOrder> getAllSupplyOrders() {
        return supplyOrderService.getAllSupplyOrders();
    }

    @PutMapping("/{id}")
    public ResponseEntity<SupplyOrder> updateSupplyOrder(@PathVariable Long id, @RequestBody SupplyOrder updatedSupplyOrder) {
        SupplyOrder updatedOrder = supplyOrderService.updateSupplyOrder(id, updatedSupplyOrder);
        return ResponseEntity.ok(updatedOrder);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSupplyOrder(@PathVariable Long id) {
        supplyOrderService.deleteSupplyOrder(id);
        return ResponseEntity.noContent().build();
    }

}
