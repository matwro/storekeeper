package com.example.inventory_management.model;

public enum OrderStatus {
    ACTIVE,
    COMPLETED,
    CANCELED
}