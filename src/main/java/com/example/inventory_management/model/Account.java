package com.example.inventory_management.model;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name; // Nazwa konta, może być przydatna jeśli jest wiele kont

    private BigDecimal balance;

    public Account() {}

    public Account(String name, BigDecimal initialBalance) {
        this.name = name;
        this.balance = initialBalance;
    }

    public void increaseBalance(BigDecimal amount) {
        this.balance = this.balance.add(amount);
    }

    public void decreaseBalance(BigDecimal amount) {
        this.balance = this.balance.subtract(amount);
    }
}
